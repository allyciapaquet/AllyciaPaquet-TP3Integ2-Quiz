// LA LISTE DES QUESTIONS 

let donneesEntrantes = `
[
	{
		"question":"En quel année le Titanic à coulé ?",
		"reponses":["Il n'a jamais coulé","En 1940","En 1912"], 
		"reponse":2,
		"bonneReponse": "En 1912"
	},
	{
		"question":"Quel est l'anime le plus regardé ?",
		"reponses":["One Piece","Naruto","Attack on Titan"], 
		"reponse":0,
    	"bonneReponse": "One Piece"
	},
    {
		"question":"Quel couleur fait le mélange de bleu et rouge ?",
		"reponses":["Vert","Violet","Orange"], 
		"reponse":1,
    	"bonneReponse": "Violet"
	},
    {
		"question":"Quel est le prénom féminin le plus donné au monde ?",
		"reponses":["Roxanne","Sarah","Sofia"], 
		"reponse":2,
    	"bonneReponse": "Sofia"
	},
    {
		"question":"Quelle main est la plus forte au poker ?",
		"reponses":["Quinte Flush Royale","Full","Double Paire"], 
		"reponse":0,
    	"bonneReponse": "Quinte Flush Royale"
	}	
]
`
// CONSTANTES, TABLEAU ET AUTRES
const alphanum = /^[a-zA-Z0-9]+$/
const donnees = {}
const questions = JSON.parse(donneesEntrantes)
let questionCourante = 0
donnees.reponses = []
$('#tableau').hide()
$('#accordeon').hide()
$('#dialog').hide()
$('#dialog2').hide()
// $('#infoUtilisateur').hide()


// VALIDATION DES INFO DU FORMULAIRE 

$(function ajouterValidation() { 
	$('#questionnaire').validate({
		rules: {
			prenom: "required",
			nom: "required",
			date_naissance: "required",
			choix: "required"
		},
		messages: { 
			prenom: "Veuillez entrer un prénom",
			nom: "Veuillez entrer un nom",
			date_naissance: "La date de naissance est requise"
		},
		submitHandler: function(){
			donnees.prenom = $('#prenom').val()
			donnees.nom = $('#nom').val()
			donnees.date_naissance = $('#date_naissance').val()
			donnees.choix = $('#choix').val()
			$("#quiz").html('');
			CreerQuiz();
		},
        showErrors: function(errorMap, errorList) {
        let est_soumis = true;
        if (est_soumis) {
            var sommaire = "Vous avez les erreurs: \n";
            $.each(errorList, function() { sommaire += " * " + this.message + "\n"; });
            est_soumis = false;
        }
        this.defaultShowErrors();
        },
        invalidHandler: function(form, validator) {
          est_soumis = true;
    
        }})});


// CALCULER L'AGE AVEC LA DATE ENTREE

function trouverAge(dateString) {
	let dateNow = new Date();
	let date_naissance = new Date(dateString);
	let age = dateNow.getFullYear() - date_naissance.getFullYear();
	let m = dateNow.getMonth() - date_naissance.getMonth();
if (m < 0 || (m === 0 && dateNow.getDate() < date_naissance.getDate())) {
age--;
}
return age;
}	


// CREATION DU QUIZ 

	function CreerQuiz(){
		afficherQuestionQuiz()
	}

	function afficherQuestionQuiz(){
			const section = $('#quiz')
			section.html('')
			question = questions[questionCourante]
			section.append('<h1>' + question.question + '</h1>')
			$('h1').attr('class', 'font-weight-bold')
			for(let i = 0; i < question.reponses.length; i++) {
			
			const reponse = $('<input></input>')
			reponse.attr('type', 'radio')
			       .attr('name', 'question')
			       .attr('id','radio'+1)
			       .attr('value', i)
				   
			const libelle = $('<label>' + question.reponses[i] + '</label>')
			libelle.attr("for","radio"+1)
			const appercu = $('<fieldset></fieldset>')
			appercu.append(reponse).animate({color: "#d88e8e;", opacity: "0.5",letterSpacing: "+=13px"});
			appercu.append(libelle).animate({color: "#d88e8e;", opacity: "3",letterSpacing: "-=10px"});
			section.append(appercu)
		 	}
		 	const bouton = $('<button>Question suivante</button>').css('background-color','pink')
		 	bouton.attr('id', 'suivante')
		 	bouton.on('click', function(){
			const reponse = $('input[name=question]:checked').val()
			questions[questionCourante].reponseFournie	= parseInt(reponse)
			questionCourante += 1
			if(questionCourante >= questions.length){
					afficherRapport()
		 		}
				else{
					afficherQuestionQuiz()
				}
			})
        		$('#quiz').append(bouton)
	}

// LE RAPPORT AVEC LE DATATABLE ET LACCORDEON

function afficherRapport () {
	// mettre le quiz dans le html
	$('#quiz').html('')

	// afficher le datatable
	$(function(){
		$("#tableau").dataTable();
	  })
	// $('#tableau').DataTable();
	$('#tableau').show()
	
	for (let i = 0; i < questions.length; i++) {
        let questionCourante = questions[i]
        const tr = $('<tr></tr>')
        tr.append('<td>' + (i + 1) + '</td>') 
        tr.append('<td>' + questionCourante.question + '</td>')

        if (questionCourante.reponse === questionCourante.reponseFournie) {
          	tr.append('<td>' + '&#10004;' + '</td>') // si la reponse est bonne

			$("#dialog").modal() // la modale qui apparait si le quiz a ete reussi
        } else {
          	tr.append('<td>X</td>') // si la reponse est mauvaise
		  
		  	$("#dialog2").modal({ // la modale qui apparait si le quiz n'a pas ete reussi
				fadeDuration: 100
		  	})
        };
        	$('tbody').append(tr)
      	}
	    	$('#accordeon').show()
	    	$( function() {
			  $( "#accordeon").accordion({
			    collapsible: true
			  });
			  AfficherProfil()
	    });
	}

function bonneReponse(){
  if(questionCourante.reponse === questionCourante.reponseFournie){
    return true 
  }
  bonneReponse =+1
}

// AFFICHER LE PROFIL DE L'UTILISATEUR

function AfficherProfil() {
	$('main').append('<h2>', 'Prénom : ' + donnees.prenom + '</h2>')
	$('main').append('<h2>', 'Nom : ' + donnees.nom + '</h2>')
	$('main').append('<h2>', 'Date de naissance : ' + donnees.date_naissance + '</h2>')
	$('main').append('<h2>', 'Statut : ' + donnees.choix + '</h2>')
	}
	
// JQUERY VALIDATOR 

//https://www.pierrefay.fr/blog/jquery-validate-formulaire-validation-tutoriel.html
$.validator.addMethod(
  "regex",
  function (value, element, regexp) {
    if (regexp.constructor != RegExp)
      regexp = new RegExp(regexp);
    else if (regexp.global)
      regexp.lastIndex = 0;
    return this.optional(element) || regexp.test(value);
  }, "erreur expression reguliere"
);

jQuery.validator.addMethod("alphanumeric", function (value, element) {
  return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please");







